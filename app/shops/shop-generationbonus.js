progress.shopsystem.gui.displayFragmentBaseStatistic = false;

shopsystem.shops['generationbonus'] = {
	values: {
		index: 0,
		active: false,
		generationNeeded: 50,
		generationBonus: 1,
		generationNeededUpdateValue: 25,
		generationBonusUpdateValue: 2,
		displayed: false,
		buyable: false,
		element: false
	},
	priority: 3,
	visible: false,
	display: function(){
		var output = "";
	
		if(this.visible && this.values.index < this.pricing.length){
			var buyable = shopsystem.values.currentFragments >= this.pricing[this.values.index];
			var action = "shopsystem.buy('generationbonus');";
			var text = "("+ this.priority +") "+ engine.texts.displayText("gui","shopbutton003") + (this.values.index+1) +" - "+ this.pricing[this.values.index] +" "+ engine.texts.displayText("gui","ui006");
			var tooltip = engine.texts.displayText("gui", "shopbuttontooltip003");
			
			output += gui.displayShopButton(buyable, action, text, tooltip);
			
			//check if button is shown for the first time
			if(!this.displayed){
				this.displayed = true;
				shopsystem.values.shopButtonsChanged = true;
			}
			
			//check if buyable state has changed
			if(buyable != this.buyable){
				this.buyable = buyable;
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		return output;
	},
	displayInfo: function(){
		if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard013");
			var value = this.values.generationNeeded + " (+"+ this.values.generationBonus + ")";
			//display statistic			
			gui.displayStatistic(this.values.element, info, value);
		}
	},
	update: function(){
		if(this.visible){
			//enough money
			if(shopsystem.values.currentFragments >= this.pricing[this.values.index]){
				//pay
				shopsystem.values.currentFragments -= this.pricing[this.values.index];
				this.values.index++;
				
				//activate or adjust
				if(this.values.active){
					//adjust fragment generation needed value
					this.values.generationNeeded += this.values.generationNeededUpdateValue;
					//adjust fragment generation bonus value
					this.values.generationBonus *= this.values.generationBonusUpdateValue;
				} else {
					this.values.active = true;
				}
			
				updateMessage = engine.texts.displayText("gui","shopmessage003");
				updateMessage = updateMessage.replace("###1###", this.values.index);
				displayProgressMessage(updateMessage);
				
				//element is not given
				if(this.values.element == false){
					this.values.element = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, false, "scoreboardItem-"+ gui.values.currentLayout);
				}
				this.displayInfo();
				
				statistics.shopsystem.updatesBought++;
				
				//display statistics
				progress.shopsystem.gui.displayFragmentBaseStatistic = true;
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	},
	pricing: [
		50,
		100,	//2
		200,
		400,	
		800,	//5
		1600,
		3200,
		6400,
		12800,
		25600,	//10
		51200
	],
	getBonusAmount: function(){
		//check if it is active
		if(this.values.active){
			return Math.floor(Math.floor((statistics.game.currentGenerations / this.values.generationNeeded)) * this.values.generationBonus);
		} else {
			return 0;
		}
	}
}
progress.shopsystem.gui.displayVelocityStatistic = false;

shopsystem.shops['velocity'] = {
	values: {
		index: 0,
		autoSpeedValue: 660,
		currentAutoSpeed: 660,
		autoSpeedUpdateValue: 20,
		displayed: false,
		buyable: false,
		element: false
	},
	priority: 1,
	visible: false,
	display: function(){
		var output = "";
		
		if(this.visible && this.values.index < this.pricing.length){
			var buyable = shopsystem.values.currentFragments >= this.pricing[this.values.index];
			var action = "shopsystem.buy('velocity');";
			var text = "("+ this.priority +") "+ engine.texts.displayText("gui","shopbutton001") + (this.values.index+1) +" - "+ this.pricing[this.values.index] +" "+ engine.texts.displayText("gui","ui006");
			var tooltip = engine.texts.displayText("gui", "shopbuttontooltip001");
			
			output += gui.displayShopButton(buyable, action, text, tooltip);
			
			//check if button is shown for the first time
			if(!this.displayed){
				this.displayed = true;
				shopsystem.values.shopButtonsChanged = true;
			}
			
			//check if buyable state has changed
			if(buyable != this.buyable){
				this.buyable = buyable;
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		return output;
	},
	displayInfo: function(){
		if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard007");
			var value = this.velocityValue();
			//display statistic			
			gui.displayStatistic(this.values.element, info, value);
		}
	},
	update: function(){
		//speed shop available
		if(progress.shopsystem.gui.displayFragments){
			//enough money
			if(shopsystem.values.currentFragments >= this.pricing[this.values.index]){
				//pay
				shopsystem.values.currentFragments -= this.pricing[this.values.index];
				this.values.index++;
				
				//adjust autoSpeed
				this.values.autoSpeedValue = this.values.autoSpeedValue - this.values.autoSpeedUpdateValue;
				if(this.values.autoSpeedValue < 50){
					this.values.autoSpeedValue = 50;
				}
				
				this.values.currentAutoSpeed = this.values.autoSpeedValue;
				
				//restart  timer
				toggleAutoplay();
				toggleAutoplay();
			
				updateMessage = engine.texts.displayText("gui","shopmessage001");
				updateMessage = updateMessage.replace("###1###", this.values.index);
				displayProgressMessage(updateMessage);
				
				//element is not given
				if(this.values.element == false){
					this.values.element = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, false, "scoreboardItem-"+ gui.values.currentLayout);
				}
				this.displayInfo();
				
				statistics.shopsystem.updatesBought++;
				
				//display statistics
				progress.shopsystem.gui.displayVelocityStatistic = true;
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	},
	pricing: [
		5,	//640
		10,
		15,	//600
		20,
		25,	//560
		30,
		35,	//520
		40,
		45,	//480
		50,
		55,	//440
		60,
		75,	//400
		90,
		100,//360
		125,
		150,//320
		200,
		250,//280
		400,
		600,//240
		800,
		1000,//200
		1200,
		1400,//160
		1600,
		1800,//120
		2000 //100
	],
	velocityValue: function(){
		return roundNumber((1/this.values.currentAutoSpeed*10000),2)
	}
};
shopsystem.shops['upgrade'] = {
	upgrades: {
		fragmentHistory: {	
			//fragment counter history
			active: false,
			name: "shopbutton006",
			tooltip: "shopbuttontooltip006",
			price: 3000,
			visible: false,
			displayed: false,
			buyable: false,
			element: false,
			setup: function(){
				
			}				
		},
		fragmentGain: {	
			//fragment amount when one is received
			active: false,
			name: "shopbutton009",
			tooltip: "shopbuttontooltip009",
			price: 123,
			visible: false,
			displayed: false,
			buyable: false,
			element: false,
			setup: function(){
				elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, "upgrade-fragmentgain", "scoreboardItem-default");
			}				
		},
		typingExtension: {
			//add the possibility to write and get additional fragments
			active: false,
			name: "shopbutton014",
			tooltip: "shopbuttontooltip014",
			price: 2222,
			visible: false,
			displayed: false,
			buyable: false,
			element: false,
			setup: function(){
				elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, "upgrade-typingExtension", "scoreboardItem-default");
			
				typingmodule.setRandomWord();
				typingmodule.visualize();
			}
		}
		/*, changed to own shop
		changeRulesOfEngagement: {	
			//change rule 0 -> 1
			active: false,
			name: "shopbutton008",
			tooltip: "shopbuttontooltip008",
			price: 100000,
			visible: false,
			displayed: false,
			buyable: false,
			element: false,
			setup: function(){
				//set generation count
				engine.copyRuleGenerations = 64;	
				//set rule active
				engine.rulesOfEngagement = 1;
			}
		}*/
	},
	displayInfo: function(){
		/*if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard016") + this.values.amount;
			elements.setHTML(this.values.element, info);
		}*/
	},
	priority: 99,
	visible: true,
	display: function(){
		var output = "";
	
		//shop is active
		if(this.visible){
			//check if upgrades are available
			Object.keys(this.upgrades).forEach(
				function(key, index) {
					var element = shopsystem.shops['upgrade'].upgrades[key];
					
					//check if upgrade is active 
					if(!element.active){
						//check which upgrade is buyable
						var buyable = shopsystem.values.currentFragments >= element.price;
						var action = "shopsystem.buy('upgrade-"+ key +"');";
						var text = engine.texts.displayText("gui", element.name) +" - "+ element.price +" "+ engine.texts.displayText("gui","ui006");
						var tooltip = engine.texts.displayText("gui", element.tooltip);
						
						if(!element.visible && buyable){
							element.visible = true;
						}
						
						//button is shown
						if(element.visible){
							output += gui.displayShopButton(buyable, action, text, tooltip);
							
							//check if button is shown for the first time
							if(!element.displayed){
								element.displayed = true;
								shopsystem.values.shopButtonsChanged = true;
							}
							
							//check if buyable state has changed
							if(buyable != element.buyable){
								element.buyable = buyable;
								shopsystem.values.shopButtonsChanged = true;
							}
						}
					}
				}
			);
		}
		
		return output;
	},
	update: function(upgradeIndex){
		//shop is activated
		if(this.visible){
			var element = this.upgrades[upgradeIndex];
		
			//enough money
			if(shopsystem.values.currentFragments >= element.price){
				//pay
				shopsystem.values.currentFragments -= element.price
				
				//activate
				element.active = true;
				element.setup();
				
				//set statistics
				statistics.shopsystem.updatesBought++;
				
				updateMessage = engine.texts.displayText("gui","shopmessage006");
				updateMessage = updateMessage.replace("###1###", engine.texts.displayText("gui", element.name));
				displayProgressMessage(updateMessage);
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	}
}
shopsystem.shops['prestige'] = {
	values: {
		index: 0,
		amount: 0,
		displayed: false,
		buyable: false,
		element: false,
		prestigeFields: [],
		usedColumns: [],
		currentColumn: 0
	},
	priority: 10,
	visible: false,
	display: function(){
		var output = "";
	
		//displayed, max gridsize, buyable
		if(this.visible && shopsystem.shops['grid'].values.index == shopsystem.shops['grid'].pricing.length && this.values.amount < (engine.gridRows * engine.gridColumns)){
			var buyable = shopsystem.values.currentFragments >= this.pricing[this.values.index];
			var action = "shopsystem.buy('prestige');";
			var text = engine.texts.displayText("gui","shopbutton007") + (this.values.amount+1) +" - "+ this.pricing[this.values.index] +" "+ engine.texts.displayText("gui","ui006");
			var tooltip = engine.texts.displayText("gui", "shopbuttontooltip007");
			
			output += gui.displayShopButton(buyable, action, text, tooltip);
			
			//check if button is shown for the first time
			if(!this.displayed){
				this.displayed = true;
				shopsystem.values.shopButtonsChanged = true;
			}
			
			//check if buyable state has changed
			if(buyable != this.buyable){
				this.buyable = buyable;
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		return output;
	},
	displayInfo: function(){
		if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard016");
			var value = this.values.amount;
			//display statistic			
			gui.displayStatistic(this.values.element, info, value);
		}
	},
	update: function(){
		//prestigeshop is activated and grid is max
		if(this.visible && shopsystem.shops['grid'].values.index == shopsystem.shops['grid'].pricing.length){
			//enough money
			if(shopsystem.values.currentFragments >= this.pricing[this.values.index]){
				//pay
				shopsystem.values.currentFragments -= this.pricing[this.values.index];
				this.values.amount++;
				
				updateMessage = engine.texts.displayText("gui","shopmessage007");
				updateMessage = updateMessage.replace("###1###", this.values.amount);
				displayProgressMessage(updateMessage);
				
				//element is not given
				if(this.values.element == false){
					this.values.element = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, false, "scoreboardItem-"+ gui.values.currentLayout);
				}
				this.displayInfo();
				
				//field to be updated
				var fieldNumberUpdate;
				var usedColumn;
				
				//get startposition
				if(this.values.usedColumns.length == 0){
					//start with center
					usedColumn = engine.gridColumns / 2;
					this.values.usedColumns.push(usedColumn);
					//in this case: 16 + (1 * 16)
					fieldNumberUpdate = (engine.gridColumns / 2) + (this.values.prestigeFields.length * engine.gridColumns);
					this.values.currentColumn = usedColumn;
				}else if(this.values.usedColumns.length < engine.gridColumns){
					fieldNumberUpdate = this.values.currentColumn;
					countRows = 1;
					while(in_array(fieldNumberUpdate, this.values.prestigeFields)){
						//check if field already prestige
						//in first case: 16 + (1 * 32)
						fieldNumberUpdate = fieldNumberUpdate + engine.gridColumns;
						countRows++;
					}
				
					//change currentColumn
					if(countRows > this.values.usedColumns.length){
						do{
							fieldNumberUpdate = roundNumber((Math.random() * (engine.gridColumns - 1)) + 1, 0);
						}while(in_array(fieldNumberUpdate, this.values.prestigeFields))
						
						usedColumn = (fieldNumberUpdate % engine.gridColumns);
						this.values.usedColumns.push(usedColumn);
						this.values.currentColumn = usedColumn;
					}
					
				
					
				}else{
					//all other fields
					fieldNumberUpdate = 1;		
					while(in_array(fieldNumberUpdate, this.values.prestigeFields)){
						fieldNumberUpdate++;
					}
				}
				
				this.values.prestigeFields.push(fieldNumberUpdate);
		
				//display new grid
				gui.board.refreshSquare(fieldNumberUpdate);
				
				statistics.shopsystem.updatesBought++;
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	},
	pricing: [
		1000
	]
}
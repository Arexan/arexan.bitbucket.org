shopsystem.shops['turbo'] = {
	values: {
		index: 0,
		active: false,
		available: false,
		turboChance: 5000,
		turboChanceUpdateValue: 200,
		turboGenerations: 0,
		turboSpeed: 25,
		turboBase: 10,
		turboMultiplicator: 1,
		turboMultiplicatorUpdateValue: 1,
		timesUsed: 0,
		displayed: false,
		buyable: false,
		element: false
	},
	priority: 6,
	visible: false,
	activate: function(){
		if(this.values.available == true){
			this.values.turboGenerations = this.values.turboBase * this.values.turboMultiplicator;
			this.values.available = false;
			shopsystem.values.shopButtonsChanged = true;
			
			//colorize grid
			document.getElementById("board").style.border = goldenTurboGridColor;
			
			//set turbo
			shopsystem.shops['velocity'].values.currentAutoSpeed = this.values.turboSpeed;
			
			//restart  timer
			if(engine.autoplayActive){
				toggleAutoplay();
				toggleAutoplay();
			} else {
				autoplay("run");
				elements.setActive(elementGenerator.getElementByAlias("button-autoplay"), false);
			}
			
			this.values.timesUsed++;
			
			//display Scoreboard
			shopsystem.displayShops();
		}
	},
	checkAvailability: function(){
		if(this.values.active == true && (Math.random() * this.values.turboChance) < 2){
			shopsystem.shops['turbo'].values.available = true;
			shopsystem.values.shopButtonsChanged = true;
		}
	},
	checkTurboStatus: function(){
		if(this.values.turboGenerations > 1) {
			this.values.turboGenerations--;
			
			//after gamereset colorize again
			if(statistics.game.currentGenerations == 0){
				//colorize grid
				document.getElementById("board").style.border = goldenTurboGridColor;
			}
		} else if (this.values.turboGenerations == 1){
			this.values.turboGenerations--;
			
			//colorize grid to default
			document.getElementById("board").style.border = defaultGridColor;
			
			//set speed
			shopsystem.shops['velocity'].values.currentAutoSpeed = shopsystem.shops['velocity'].values.autoSpeedValue;
			
			//activate speed
			toggleAutoplay();
			toggleAutoplay();
		}
	},
	displayActivation: function(){
		var output = "";
		output += "<div>";
			output += "<div id=\"turboButton\" class=\"paybutton-"+ gui.values.currentLayout +"-bonus\" onmousedown=\"shopsystem.shops['turbo'].activate();\">"+ engine.texts.displayText("gui","ui007");
			output += "</div>";
		output += "</div>";
		output += "<div class=\"clearfix\"> </div>";
		
		return output;
	},
	display: function(){
		var output = "";
	
		if(this.visible && this.values.index < this.pricing.length){
			var buyable = shopsystem.values.currentFragments >= this.pricing[this.values.index];
			var action = "shopsystem.buy('turbo');";
			
			//display activate
			if(this.values.index == 0){
				var text = "("+ this.priority +") "+ engine.texts.displayText("gui","shopbutton010") +" - "+ this.pricing[this.values.index] +" "+ engine.texts.displayText("gui","ui006");
				var tooltip = engine.texts.displayText("gui", "shopbuttontooltip010");
			}
			//display chance
			else if((this.values.index % 2) == 1){
				var text = "("+ this.priority +") "+ engine.texts.displayText("gui","shopbutton011") +" - "+ this.pricing[this.values.index] +" "+ engine.texts.displayText("gui","ui006");
				var tooltip = engine.texts.displayText("gui", "shopbuttontooltip011");
			}
			//display generations
			else if((this.values.index % 2) == 0){
				var text = "("+ this.priority +") "+ engine.texts.displayText("gui","shopbutton012") +" - "+ this.pricing[this.values.index] +" "+ engine.texts.displayText("gui","ui006");
				var tooltip = engine.texts.displayText("gui", "shopbuttontooltip012");
			}
			
			output += gui.displayShopButton(buyable, action, text, tooltip);
			
			//check if button is shown for the first time
			if(!this.displayed){
				this.displayed = true;
				shopsystem.values.shopButtonsChanged = true;
			}
			
			//check if buyable state has changed
			if(buyable != this.buyable){
				this.buyable = buyable;
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		return output;
	},
	displayInfo: function(){
		if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard018");
			var value = this.values.timesUsed;
			//display statistic			
			gui.displayStatistic(this.values.element, info, value);
		}
	},
	update: function(){
		//shop is activated
		if(this.visible){
			//enough money
			if(this.visible && shopsystem.values.currentFragments >= this.pricing[this.values.index]){
				//pay
				shopsystem.values.currentFragments -= this.pricing[this.values.index];
				
				//activate
				if(this.values.index == 0){
					this.values.active = true;
				}
				//adjust chance
				else if((this.values.index % 2) == 1){
					this.values.turboChance -= this.values.turboChanceUpdateValue;
				}
				//adjust generations
				else if((this.values.index % 2) == 0){
					this.values.turboMultiplicator += this.values.turboMultiplicatorUpdateValue;
				}
				
				//element is not given
				if(this.values.element == false){
					this.values.element = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, false, "scoreboardItem-"+ gui.values.currentLayout);
				}
				this.displayInfo();
				
				if(this.values.index == 0){
					updateMessage = engine.texts.displayText("gui","shopmessage008");
					displayProgressMessage(updateMessage);
				}
				
				this.values.index++;
				statistics.shopsystem.updatesBought++;
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	},
	pricing: [
		22,		//usable 5000 - 15
		33,		//chance - 4800
		44,		//generations - 30
		55,		//chance - 4600
		66,		//generations - 45
		111,	//chance - 4400
		222,	//generations - 70
		333,	//chance - 4200
		444,	//generations - 85
		555,	//chance - 4000
		666,	//generations - 90
		1111,	//chance - 3800
		2222,	//generations - 105
		3333,	//chance - 3600
		4444,	//generations - 120
		5555,	//chance - 3400
		6666,	//generations - 135
		11111,	//chance - 3200
		22222	//generations - 150
	]
}
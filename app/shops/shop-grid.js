progress.shopsystem.gui.displayGridStatistic = false;

shopsystem.shops['grid'] = {
	values: {
		index: 0,
		displayed: false,
		buyable: false,
		element: false
	},
	priority: 2,
	visible: false,
	display: function(){
		var output = "";
	
		if(this.visible && this.values.index < this.pricing.length){
			var buyable = shopsystem.values.currentFragments >= this.pricing[this.values.index];
			var action = "shopsystem.buy('grid');";
			var text = "("+ this.priority +") "+ engine.texts.displayText("gui","shopbutton002") + (this.values.index+1) +" - "+ this.pricing[this.values.index] +" "+ engine.texts.displayText("gui","ui006");
			var tooltip = engine.texts.displayText("gui", "shopbuttontooltip002");
			
			output += gui.displayShopButton(buyable, action, text, tooltip);
			
			//check if button is shown for the first time
			if(!this.displayed){
				this.displayed = true;
				shopsystem.values.shopButtonsChanged = true;
			}
			
			//check if buyable state has changed
			if(buyable != this.buyable){
				this.buyable = buyable;
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		return output;
	},
	displayInfo: function(){
		if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard008");
			var value = engine.gridRows +"x"+ engine.gridColumns;
			//display statistic			
			gui.displayStatistic(this.values.element, info, value);
		}
	},
	update: function(){
		//gridshop is activated
		if(this.visible){
			//enough money
			if(shopsystem.values.currentFragments >= this.pricing[this.values.index]){
				//pay
				shopsystem.values.currentFragments -= this.pricing[this.values.index];
				this.values.index++;
				
				//update grid
				engine.gridRows++;
				engine.gridColumns++;
				
				//reset game values
				resetGame();
				resetStatistics();
				
				updateMessage = engine.texts.displayText("gui","shopmessage002");
				updateMessage = updateMessage.replace("###1###", this.values.index);
				displayProgressMessage(updateMessage);
				
				//element is not given
				if(this.values.element == false){
					this.values.element = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, false, "scoreboardItem-"+ gui.values.currentLayout);
				}
				this.displayInfo();
				
				statistics.shopsystem.updatesBought++;
				
				//display statistics
				progress.shopsystem.gui.displayGridStatistic = true;
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	},
	pricing: [
		10, 	//13
		25,	
		50,		//15
		75,	
		100,	//17	
		150,
		200,	//19
		300,
		400,	//21
		500,
		666,	//23
		700,
		800,	//25
		900,
		1001,	//27
		1337,
		1500,	//29
		2000,	//30
		2500,
		3700/*,	//32
		4900,
		6100,	//34
		8300,	
		10000,	//36
		13000,
		18000,	//38
		25000,
		30000	//40*/
	]
}
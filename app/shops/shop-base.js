progress.shopsystem.gui.displayFragmentBaseStatistic = false;

shopsystem.shops['base'] = {
	values: {
		index: 0,
		fragmentBase: 0,
		fragmentBaseUpdateValue: 1,
		displayed: false,
		buyable: false,
		element: false
	},
	priority: 5,
	visible: false,
	display: function(){
		var output = "";
	
		if(this.visible && this.values.index < this.pricing.length){
			var buyable = shopsystem.values.currentFragments >= this.pricing[this.values.index];
			var action = "shopsystem.buy('base');";
			var text = "("+ this.priority +") "+ engine.texts.displayText("gui","shopbutton005") + (this.values.index+1) +" - "+ this.pricing[this.values.index] +" "+ engine.texts.displayText("gui","ui006");
			var tooltip = engine.texts.displayText("gui", "shopbuttontooltip005");
			
			output += gui.displayShopButton(buyable, action, text, tooltip);
			
			//check if button is shown for the first time
			if(!this.displayed){
				this.displayed = true;
				shopsystem.values.shopButtonsChanged = true;
			}
			
			//check if buyable state has changed
			if(buyable != this.buyable){
				this.buyable = buyable;
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		return output;
	},
	displayInfo: function(){
		if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard015");
			var value = "+" + this.values.fragmentBase;
			//display statistic			
			gui.displayStatistic(this.values.element, info, value);
		}
	},
	update: function(){
		//shop is activated
		if(this.visible){
			//enough money
			if(this.visible && shopsystem.values.currentFragments >= this.pricing[this.values.index]){
				//pay
				shopsystem.values.currentFragments -= this.pricing[this.values.index];
				this.values.index++;
				
				//adjust base
				this.values.fragmentBase += this.values.fragmentBaseUpdateValue;
				
				//element is not given
				if(this.values.element == false){
					this.values.element = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, false, "scoreboardItem-"+ gui.values.currentLayout);
				}
				this.displayInfo();
				
				updateMessage = engine.texts.displayText("gui","shopmessage005");
				updateMessage = updateMessage.replace("###1###", this.values.index);
				displayProgressMessage(updateMessage);
				
				statistics.shopsystem.updatesBought++;
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	},
	pricing: [
		100,	//2
		250,
		500,	//4
		750,
		1000,	//6
		1250,
		1500,	//8
		1750,
		2000,	//10
		2250,
		2500,	//12
		2750,
		3000,	//14
		3250,
		3500,	//16
		3750,
		4000,	//18
		4250,
		4500	//20	
	]
}
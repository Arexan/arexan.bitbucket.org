progress.shopsystem.gui.displayFragmentBaseStatistic = false;

shopsystem.shops['rules'] = {
	values: {
		index: 0,
		displayed: false,
		buyable: false,
		element: false
	},
	priority: 7,
	visible: false,
	display: function(){
		var output = "";
	
		if(this.visible && this.values.index < this.pricing.length){
			var buyable = shopsystem.values.currentFragments >= this.pricing[this.values.index];
			var action = "shopsystem.buy('rules');";
			var text = engine.texts.displayText("gui","shopbutton013") + (this.values.index+1) +" - "+ this.pricing[this.values.index] +" "+ engine.texts.displayText("gui","ui006");
			var tooltip = engine.texts.displayText("gui", "shopbuttontooltip013");
			
			output += gui.displayShopButton(buyable, action, text, tooltip);
			
			//check if button is shown for the first time
			if(!this.displayed){
				this.displayed = true;
				shopsystem.values.shopButtonsChanged = true;
			}
			
			//check if buyable state has changed
			if(buyable != this.buyable){
				this.buyable = buyable;
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		return output;
	},
	displayInfo: function(){
		if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard019");
			var rulename;
			var value = this.values.index;
			if(value < 10){
				rulename = "rulename00" + value;
			}else{
				rulename = "rulename0" + value;
			}
			
			rulename = engine.texts.displayText("gui",rulename);
			//display statistic			
			gui.displayStatistic(this.values.element, info, rulename);
		}
	},
	update: function(){
		//shop is activated
		if(this.visible){
			//enough money
			if(this.visible && shopsystem.values.currentFragments >= this.pricing[this.values.index]){
				//pay
				shopsystem.values.currentFragments -= this.pricing[this.values.index];
				this.values.index++;
				
				//change ruleset
				engine.rulesOfEngagement = this.values.index;
				
				//element is not given
				if(this.values.element == false){
					this.values.element = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, false, "scoreboardItem-"+ gui.values.currentLayout);
				}
				this.displayInfo();
				
				updateMessage = engine.texts.displayText("gui","shopmessage009");
				updateMessage = updateMessage.replace("###1###", this.values.index);
				displayProgressMessage(updateMessage);
				
				statistics.shopsystem.updatesBought++;
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	},
	pricing: [
		100000,
		200000,
		300000,
		400000,
		500000,
		600000,
		700000
	]
}
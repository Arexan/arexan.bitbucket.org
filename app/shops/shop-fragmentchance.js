progress.shopsystem.gui.displayFragmentChanceStatistic = false;

shopsystem.shops['fragmentchance'] = {
	values: {
		index: 0,
		chance: 1000,
		chanceUpdateValue: 20,
		displayed: false,
		buyable: false,
		element: false
	},
	priority: 4,
	visible: false,
	display: function(){
		var output = "";
	
		if(this.visible && this.values.index < this.pricing.length){
			var buyable = shopsystem.values.currentFragments >= this.pricing[this.values.index];
			var action = "shopsystem.buy('fragmentchance');";
			var text = "("+ this.priority +") "+ engine.texts.displayText("gui","shopbutton004") + (this.values.index+1) +" - "+ this.pricing[this.values.index] +" "+ engine.texts.displayText("gui","ui006");
			var tooltip = engine.texts.displayText("gui", "shopbuttontooltip004");
			
			output += gui.displayShopButton(buyable, action, text, tooltip);
			
			//check if button is shown for the first time
			if(!this.displayed){
				this.displayed = true;
				shopsystem.values.shopButtonsChanged = true;
			}
			
			//check if buyable state has changed
			if(buyable != this.buyable){
				this.buyable = buyable;
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		return output;
	},
	displayInfo: function(){
		if(this.values.element != false){
			var info = engine.texts.displayText("gui","scoreboard012");
			var value = "1/" + this.values.chance;
			//display statistic			
			gui.displayStatistic(this.values.element, info, value);
		}
	},
	update: function(){
		//shop available
		if(this.visible){
			//enough money
			if(shopsystem.values.currentFragments >= this.pricing[this.values.index]){
				//pay
				shopsystem.values.currentFragments -= this.pricing[this.values.index];
				this.values.index++;
				
				//adjust fragment chance
				this.values.chance -= this.values.chanceUpdateValue;
			
				updateMessage = engine.texts.displayText("gui","shopmessage004");
				updateMessage = updateMessage.replace("###1###", this.values.index);
				displayProgressMessage(updateMessage);
				
				//element is not given
				if(this.values.element == false){
					this.values.element = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("scoreboard"), false, false, "scoreboardItem-"+ gui.values.currentLayout);
				}
				this.displayInfo();
				
				statistics.shopsystem.updatesBought++;
				
				//display statistics
				progress.shopsystem.gui.displayFragmentChanceStatistic = true;
				
				//set that buttons have changed
				shopsystem.values.shopButtonsChanged = true;
			}
		}
		
		displayScoreboardGUI();
	},
	pricing: [
		80,
		160,
		240,
		450,
		700,	//5
		900,
		1100,
		1300,
		1600,
		2000,	//10
		2500,
		3000,
		3500,
		4000,
		4500,	//15
		5000,
		5500,
		6000,
		6500,
		7000,	//20
		7500,
		8000,
		8500,
		9000,
		11111	//25
	]
}
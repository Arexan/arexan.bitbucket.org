//graphical user interface object
gui = {
	values: {
		currentLayout: "default"
	},
	setElementTextById: function(elementName, text){
		var element = document.getElementById(elementName);
		var elementType = element.nodeName;
		
		if (elementType == "INPUT"){
			element.value = text;
		} else if(elementType == "DIV"){
			element.innerHTML = text;
		}
	},
	displayShopButton: function(buyable, action, text, tooltip){
		var output = "";
	
		//check if button is payable
		if(buyable){
			buttonClass = "paybutton-"+ this.values.currentLayout;
		} else {
			buttonClass = "paybutton-"+ this.values.currentLayout +"-off";
		}
	
		//display html
		output += "<div>";
			output += "<div class=\""+ buttonClass +"\" onClick=\""+ action +"\" title=\""+ tooltip +"\">"+ text +"</div>";
		output += "</div>";
		output += "<div class=\"clearfix\"> </div>";
		
		return output;
	},
	displayHistory: function(message){
		var elementName = elementGenerator.addElementWithClassname(elementGenerator.getElementByAlias("history-container"), true, false, "historyContainerItem-"+ this.values.currentLayout);
		elements.setHTML(elementName, message);
	},
	displayStatistic: function(element, info, value){
		//get html element
		var htmlElement = document.getElementById(element);
		
		//check if childs are generated
		if(htmlElement.hasChildNodes()){
			//get info
			var htmlInfoId = htmlElement.firstChild.id;
			//get value
			var htmlValueId = htmlElement.lastChild.id;
		} else {
			//create info
			var htmlInfoId = elementGenerator.addElementWithClassname(element, false, false, "info-"+ gui.values.currentLayout);
			//create value
			var htmlValueId = elementGenerator.addElementWithClassname(element, false, false, "value-"+ gui.values.currentLayout);
		}
		
		
		//set info
		elements.setHTML(htmlInfoId, info);
		
		//set value
		elements.setHTML(htmlValueId, value);
	},
	board: {
		refreshSquare: function(number){
			document.getElementById('square'+ getInputFieldNumber(number)).className += " prestige";
		}
	}
}

//display grid
function buildGrid(numberOfInputs){
	var output = "";
	
	for(var i = 1; i <= numberOfInputs; i++){
		var fieldNumber = getInputFieldNumber(i);
		var classes = "square-"+ gui.values.currentLayout;
		
		//display prestige fields
		if(shopsystem.shops['prestige'].visible && shopsystem.shops['prestige'].values.amount > 0){
			
			if(in_array(fieldNumber, shopsystem.shops['prestige'].values.prestigeFields)){
				classes += " prestige";
			}
			
			
		}
		
		output += "\n<div id=\"square" + fieldNumber + "\" class=\""+ classes +"\" style=\"width:"+ 640/(engine.gridColumns) +"px; height:"+ 640/(engine.gridRows) +"px;\"><input type=\"hidden\" id=\"inputSquare" + fieldNumber + "\" value=0 /></div>";
	}
	
	return output;
}

function in_array(needle, haystack) {
    for(var i in haystack) {
        if(haystack[i] == needle) return true;
    }
    return false;
}



//deactivate progress message
function hideProgressMessage(){
	document.getElementById("progressMessage").style.display = 'none';
}

//display progress message
function displayProgressMessage(message){
	document.getElementById("progressMessage").innerHTML = message;
	document.getElementById("progressMessage").style.display = 'block';
	
	setTimeout(hideProgressMessage, 2500);
}

//deactivate info message
function hideInfoMessage(){
	document.getElementById("infoMessage").style.display = 'none';
}

//display info message
function displayInfoMessage(message){
	document.getElementById("infoMessage").innerHTML = message;
	document.getElementById("infoMessage").style.display = 'block';
	
	engine.infoMessageTimer = setTimeout(hideInfoMessage, 5000);
}

//display scoreboard
function displayScoreboardGUI(){
	if(progress.gui.displayStatistics){
		//main statistics
		gui.displayStatistic(elementGenerator.getElementByAlias("gameinfo-totalgames"),  engine.texts.displayText("gui","scoreboard001"), statistics.game.gamesResetAutomatically);
		gui.displayStatistic(elementGenerator.getElementByAlias("gameinfo-totalresets"), engine.texts.displayText("gui","scoreboard002"), statistics.game.gamesResetManually);
		
		//current fragment count
		if(progress.shopsystem.gui.displayFragments){
			var info = engine.texts.displayText("gui","scoreboard011");
			var value = shopsystem.values.currentFragments;
			gui.displayStatistic(elementGenerator.getElementByAlias("current-fragments"), info, value);
		}
		
		//display shop statistics
		if(progress.shopsystem.gui.displayFragments){
			Object.keys(shopsystem.shops).forEach(
				function(key, index) {
					var element = shopsystem.shops[key];
					
					//shop is visible
					if(element.visible){
						element.displayInfo();
					}
				}
			);
		}
	
		//display fragment gain
		if(shopsystem.shops['upgrade'].upgrades.fragmentGain.active){
			var info = engine.texts.displayText("gui","scoreboard017");
			var value = shopsystem.getFragmentsPerGeneration(false, false);
			gui.displayStatistic(elementGenerator.getElementByAlias("upgrade-fragmentgain"), info, value);
		}
		
		//display typing addon highest streak
		if(shopsystem.shops['upgrade'].upgrades.typingExtension.active){
			var info = engine.texts.displayText("gui","scoreboard020");
			var value = typingmodule.values.highestStreak;
			gui.displayStatistic(elementGenerator.getElementByAlias("upgrade-typingExtension"), info, value);
		}
		
	
		/*
		document.getElementById("min-ratio").innerHTML = statistics.game.lowestRatio;
		document.getElementById("max-ratio").innerHTML = statistics.game.highestRatio;
		document.getElementById("min-rounds").innerHTML = statistics.game.lowestGeneration;
		document.getElementById("max-rounds").innerHTML = statistics.game.highestGeneration;
		document.getElementById("min-rounds").innerHTML = statistics.game.lowestGeneration;
		document.getElementById("max-rounds").innerHTML = statistics.game.highestGeneration;
		
		if(progress.shopsystem.gui.displayUpdatesStatistic){
			document.getElementById("updates").innerHTML = statistics.shopsystem.updatesBought;
		}*/
		
		//display shops if fragments have changed
		if(shopsystem.fragmentsLastGeneration != shopsystem.values.currentFragments){
			shopsystem.displayShops();
			shopsystem.values.fragmentsLastGeneration = shopsystem.values.currentFragments;
		}
	}
}

//keyboard usage
function checkKey(e) {
    var event = window.event ? window.event : e;
	var keyvalue = keymapper.getKeyValueById(event.keyCode);
	
    //console.log(event.keyCode);
	//console.log(keyvalue);
	
	//next round
	if(keyvalue == "enter" || keyvalue == ","){
		generateFields();
	}
	//reset
	else if(keyvalue == "-"){
		if(progress.gui.displayReset == true){
			resetGame(false);
		}
	}
	//autoplay
	else if(keyvalue == "."){
		if(progress.gui.displayAutoPlay == true){
			toggleAutoplay();
		}
	}
	//turbo
	else if(keyvalue == "#"){
		shopsystem.shops['turbo'].activate();
	}
	//SHOPS
	else if(keyvalue == "1" || keyvalue == "num-1"){
		//speed
		shopsystem.buy('velocity');
	}else if(keyvalue == "2" || keyvalue == "num-2"){
		//grid
		shopsystem.buy('grid');
	}else if(keyvalue == "3" || keyvalue == "num-3"){
		//fragment bonus generations
		shopsystem.buy('generationbonus');
	}else if(keyvalue == "4" || keyvalue == "num-4"){
		//fragment chance
		shopsystem.buy('fragmentchance');
	}else if(keyvalue == "5" || keyvalue == "num-5"){
		//fragment base
		shopsystem.buy('base');
	}else if(keyvalue == "6" || keyvalue == "num-6"){
		//turbo
		shopsystem.buy('turbo');
	
	}else if(keyvalue == "0" || keyvalue == "num-0"){
		//prestige
		shopsystem.buy('prestige');
	}
	
	//check typing addon active and is a-z 
	else if(shopsystem.shops['upgrade'].upgrades.typingExtension.active && keyvalue != false && keyvalue.length == 1 && keyvalue.match(/[a-z]/i)){
		typingmodule.checkCurrentWord(keyvalue);
	}
}
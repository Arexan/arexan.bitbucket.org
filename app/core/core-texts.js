engine.texts = {
	initialize: function(){
		//title
		document.title = this.displayText("game","title");
		gui.setElementTextById("title",this.displayText("game","title"));
	
		//buttons
		gui.setElementTextById("click",this.displayText("gui","button001"));
		
		//add language selectors
		var languageSelector = "";
		
		Object.keys(engine.texts.values.languages).forEach(
			function(key, index) {
				if(typeof(elementGenerator.getElementByAlias('languageSelector-'+ key)) == "undefined"){
					elementGenerator.addElementWithClassname("page-top", false, "languageSelector-"+ key, "languageselector-"+ gui.values.currentLayout);
				}
				
				languageSelector = elementGenerator.getElementByAlias("languageSelector-"+ key);
				elements.setHTML(languageSelector, key);
				elements.addEventListener(languageSelector, "mouseup", "engine.texts.changeLanguage", key);
			}
		);
	},
	changeLanguage: function(languageCode){
		//check if language is given
		if(engine.functions.objectExists(this.values.languages[languageCode])){
			//change language
			this.values.language = languageCode;
			
			//reinitialize language
			this.initialize();
		}
	},
	displayText: function(group, identifier){
		return this.values.languages[this.values.language][group][identifier];
	},
	values: {
		language: "en",
		languages: {
			en: {
				//game
				game: {
					"title": "Conway's Game of Life"
				},
				//gui
				gui: {
					"button001": "next generation (,)",
					"button002": "toggle autoplay (.)",
					"button003": "reset game (-)",
					
					"scoreboard001": "Games played: ",
					"scoreboard002": "Games reset: ",
					"scoreboard003": "Max Generations: ",
					"scoreboard004": "Min Generations: ",
					"scoreboard005": "Max Ratio: ",
					"scoreboard006": "Min Ratio: ",
					"scoreboard007": "Velocity: ",
					"scoreboard008": "Grid Size: ",
					"scoreboard009": "Updates: ",
					"scoreboard010": "Golden-Turbos: ",
					"scoreboard011": "Fragments <i id=\"money-icon\" class=\"fa fa-money\"></i>: ",
					"scoreboard012": "Chance: ",
					"scoreboard013": "Bonus active at: ",
					"scoreboard014": "Per Generation: ",
					"scoreboard015": "Fragment Base: ",
					"scoreboard016": "Prestige Level: ",
					"scoreboard017": "Fragment Gain: ",
					"scoreboard018": "Turbos used: ",
					"scoreboard019": "Ruleset: ",
					"scoreboard020": "Highest Streak: ",
					
					"ui001": "Generation: ",
					"ui002": "Fields alive: ",
					"ui003": "ratio: ",
					"ui004": "Manual reset!",
					"ui005": "Generation ",
					"ui006": "<i class=\"fa fa-money\"></i>",
					"ui007": "(#) Golden Turbo - BONUS",
					"ui008": "History",
					"ui009": "Statistic",
					"ui010": "Possible bonus: ###1###: ",
					"ui011": " On a roll x",
					
					"message001": "Infinite loop, please reset game.",
					"message002": "No lives left, please reset game.",
					"message003": "Do you want to leave this page?",
					
					"progressmessage001": "Reset Button Unlocked!",
					"progressmessage002": "Generations Unlocked!",
					"progressmessage003": "Game Info Unlocked!",
					"progressmessage004": "Autoplay Unlocked!",
					"progressmessage005": "Auto Reset Unlocked!",
					"progressmessage006": "History Unlocked!",
					"progressmessage007": "Scoreboard Unlocked!",
					"progressmessage008": "Fragments Unlocked!",
					
					"shopbutton001": "Velocity Boost ",
					"shopbutton002": "Extend Grid ",
					"shopbutton003": "Generation Bonus ",
					"shopbutton004": "Fragment Chance ",
					"shopbutton005": "Fragment Base ",
					"shopbutton006": "Fragment History ",
					"shopbutton007": "(0) Buy Prestige Field ",
					"shopbutton008": "Change Rules Of Engagement ",
					"shopbutton009": "Fragment Gain ",
					"shopbutton010": "Unlock Turbo ",
					"shopbutton011": "Turbo Chance ",
					"shopbutton012": "Turbo Generations ",
					"shopbutton013": "Change Ruleset ",
					"shopbutton014": "Typing Module ",
					
					"shopbuttontooltip001": "Increases the autoplay speed. Go Faster!",
					"shopbuttontooltip002": "Increases the grid size. More action!",
					"shopbuttontooltip003": "After a given generation the fragment amount that is received once one is found gets a certain bonus.",
					"shopbuttontooltip004": "Increases the chance that a fragment is found.",
					"shopbuttontooltip005": "Increases the base amount of fragments that are received once one is found.",
					"shopbuttontooltip006": "Display how much fragments were received in the last game.",
					"shopbuttontooltip007": "Want some thing fancy?",
					"shopbuttontooltip008": "Changes the game rule",
					"shopbuttontooltip009": "Displays the amount of fragments are received if one is found in the generation.",
					"shopbuttontooltip010": "Unlock a random occuring turbo.",
					"shopbuttontooltip011": "Increase the chance to get a random turbo.",
					"shopbuttontooltip012": "Increase the time that the random turbo runs.",
					"shopbuttontooltip013": "Changes the game rules for life and death.",
					"shopbuttontooltip014": "Get fragments for correctness.",
					
					"shopmessage001": "Speedboost ###1### Unlocked!",
					"shopmessage002": "Extend Grid ###1### Unlocked (Statistics Reset)!",
					"shopmessage003": "Generation Bonus ###1### Unlocked!",
					"shopmessage004": "Fragment Chance ###1### Unlocked!",
					"shopmessage005": "Fragment Base ###1### Unlocked!",
					"shopmessage006": "###1### Unlocked!",
					"shopmessage007": "Prestige ###1### Unlocked!",
					"shopmessage008": "Turbo activated!",
					"shopmessage009": "Games ruleset changed!",
					
					"tooltip001": "Write the highlighted character on the keyboard to get extra fragments.",
					
					"rulename001": "3/3 rule",
					"rulename002": "13/3 rule",
					"rulename003": "35/3 rule",
					"rulename004": "2/3 rule",
					"rulename005": "24/3 rule",
					"rulename006": "245/3 rule",
					"rulename007": "125/36 rule",
					
					"cheatprotection001": "Fragment Generation stopped for ###1### Game/s!"
				},
				typingmodule: {
					0: "word",
					1: "the",
					2: "first",
					3: "opportunity",
					4: "chance",
					5: "vanity",
					6: "coolness",
					7: "birthday",
					8: "singularity",
					9: "browser",
					10: "canyon",
					11: "pentadecathlon",
					12: "spaceship",
					13: "beacon",
					14: "glider",
					15: "beehive",
					16: "speed",
					17: "generation",
					18: "life",
					19: "game",
					20: "conway",
					21: "three",
					22: "four",
					23: "six",
					24: "module",
					25: "protection",
					26: "fragments",
					27: "currency",
					28: "famous",
					29: "museum",
					30: "valuable",
					31: "zoo",
					32: "telephone",
					33: "ground",
					34: "internet",
					35: "voice",
					36: "road",
					37: "wrong",
					38: "correctness",
					39: "title",
					40: "button",
					41: "rules"
				}
			},
			de: {
				//game
				game: {
					"title": "Conway's Spiel des Lebens"
				},
				//gui
				gui: {
					"button001": "Naechste Generation (,)",
					"button002": "Aktiviere Automatik (.)",
					"button003": "Neustart (-)",
					
					"scoreboard001": "Spiele: ",
					"scoreboard002": "Neustarts: ",
					"scoreboard003": "Max Generationen: ",
					"scoreboard004": "Min Generationen: ",
					"scoreboard005": "Max Ratio: ",
					"scoreboard006": "Min Ratio: ",
					"scoreboard007": "Geschwindigkeit: ",
					"scoreboard008": "Spielfeld: ",
					"scoreboard009": "Verbesserungen: ",
					"scoreboard010": "Goldene-Turbos: ",
					"scoreboard011": "Fragmente <i id=\"money-icon\" class=\"fa fa-money\"></i>: ",
					"scoreboard012": "Chance: ",
					"scoreboard013": "Bonus alle: ",
					"scoreboard014": "Pro Generation: ",
					"scoreboard015": "Fragment Basis: ",
					"scoreboard016": "Prestige Level: ",
					"scoreboard017": "Fragment Wert: ",
					"scoreboard018": "Turbos genutzt: ",
					"scoreboard019": "Ruleset: ",
					"scoreboard020": "Highest Streak: ",
					
					"ui001": "Generation: ",
					"ui002": "Felder lebendig: ",
					"ui003": "ratio: ",
					"ui004": "Manueller neustart!",
					"ui005": "Generation ",
					"ui006": "<i class=\"fa fa-money\"></i>",
					"ui007": "(#) Goldener Turbo - BONUS",
					"ui008": "Historie",
					"ui009": "Statistik",
					"ui010": "Possible bonus ###1###: ",
					"ui011": " On a roll x",
					
					"message001": "Unendliches Spiel, bitte neustarten!",
					"message002": "Keine Lebenden Felder uebrig, bitte neustarten.",
					"message003": "Die Seite wirklich verlassen?",
					
					"progressmessage001": "Neustarten freigeschaltet!",
					"progressmessage002": "Generationen freigeschaltet!",
					"progressmessage003": "Spiel Info freigeschaltet!",
					"progressmessage004": "Automatisches spielen freigeschaltet!",
					"progressmessage005": "Automatisches neustarten freigeschaltet!",
					"progressmessage006": "Historie freigeschaltet!",
					"progressmessage007": "Fortschrittanzeige freigeschaltet!",
					"progressmessage008": "Fragmente freigeschaltet!",
					
					"shopbutton001": "Geschwindigkeits Schub ",
					"shopbutton002": "Spielfeld Erweiterung ",
					"shopbutton003": "Generations Bonus ",
					"shopbutton004": "Fragment Chance ",
					"shopbutton005": "Fragment Basis ",
					"shopbutton006": "Fragment Historie ",
					"shopbutton007": "(0) Prestige Feld kaufen ",
					"shopbutton008": "Rules Of Engagement ändern ",
					"shopbutton009": "Fragment Anzeige ",
					"shopbutton010": "Turbo freischalten ",
					"shopbutton011": "Turbo Chance ",
					"shopbutton012": "Turbo Generationen ",
					"shopbutton013": "Change Ruleset ",
					"shopbutton014": "Typing Module ",
					
					
					"shopbuttontooltip001": "Increases the autoplay speed. Go Faster!",
					"shopbuttontooltip002": "Increases the grid size. More action!",
					"shopbuttontooltip003": "After a given generation the fragment amount that is received once one is found gets a certain bonus.",
					"shopbuttontooltip004": "Increases the chance that a fragment is found.",
					"shopbuttontooltip005": "Increases the base amount of fragments that are received once one is found.",
					"shopbuttontooltip006": "Display how much fragments were received in the last game.",
					"shopbuttontooltip007": "Want some thing fancy?",
					"shopbuttontooltip008": "Changes the game rule",
					"shopbuttontooltip009": "Displays the amount of fragments are received if one is found in the generation.",
					"shopbuttontooltip010": "Unlock a random occuring turbo.",
					"shopbuttontooltip011": "Increase the chance to get a random turbo.",
					"shopbuttontooltip012": "Increase the time that the random turbo runs.",
					"shopbuttontooltip013": "Changes the game rules for life and death.",
					"shopbuttontooltip014": "Get fragments for correctness.",
					
					"shopmessage001": "Geschwindigkeits Bonus ###1### freigeschaltet!",
					"shopmessage002": "Spielfeld Erweiterung ###1### freigeschaltet (Statistik zurueckgesetzt)!",
					"shopmessage003": "Generations Bonus ###1### freigeschaltet!",
					"shopmessage004": "Fragment Chance ###1### freigeschaltet!",
					"shopmessage005": "Fragment Basis ###1### freigeschaltet!",
					"shopmessage006": "###1### freigeschaltet!",
					"shopmessage007": "Prestige ###1### freigeschaltet!",
					"shopmessage008": "Turbo aktiviert!",
					"shopmessage009": "Games ruleset changed!",
					
					"tooltip001": "Write the highlighted character on the keyboard to get extra fragments.",
					
					"rulename001": "3/3 rule",
					"rulename002": "13/3 rule",
					"rulename003": "35/3 rule",
					"rulename004": "2/3 rule",
					"rulename005": "24/3 rule",
					"rulename006": "245/3 rule",
					"rulename007": "125/36 rule",
					
					"cheatprotection001": "Fragment Generierung fuer ###1### Spiel/e gestoppt!"
				},
				typingmodule: {
					0: "word",
					1: "the",
					2: "first",
					3: "opportunity",
					4: "chance",
					5: "vanity",
					6: "coolness",
					7: "birthday",
					8: "singularity",
					9: "browser",
					10: "canyon",
					11: "pentadecathlon",
					12: "spaceship",
					13: "beacon",
					14: "glider",
					15: "beehive",
					16: "speed",
					17: "generation",
					18: "life",
					19: "game",
					20: "conway",
					21: "three",
					22: "four",
					23: "six",
					24: "module",
					25: "protection",
					26: "fragments",
					27: "currency",
					28: "famous",
					29: "museum",
					30: "valuable",
					31: "zoo",
					32: "telephone",
					33: "ground",
					34: "internet",
					35: "voice",
					36: "road",
					37: "wrong",
					38: "correctness",
					39: "title",
					40: "button"
				}
			}
		}
	}
}
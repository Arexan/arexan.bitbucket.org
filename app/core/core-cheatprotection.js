cheatProtection = {
	values: {
		blockFragments: false,
		blockedFragmentsLastRound: false,
		blockFragmentsGenerations: 0,
		fastResets: 0
	},
	checkExploits: function() {
		//manual resets for fragment boost >= 3
		if(cheatProtection.values.fastResets >= 3){
			cheatProtection.values.blockFragments = true;
			cheatProtection.values.blockFragmentsGenerations += 3;
			
			//notice
			notice = engine.texts.displayText("gui","cheatprotection001");
			notice = notice.replace("###1###", cheatProtection.values.blockFragmentsGenerations);
			displayInfoMessage(notice);
		}
	},
	checkGenerations: function(){
		//decrease block fragments generation per game
		if(statistics.game.currentGenerations > 0 && cheatProtection.values.blockFragmentsGenerations > 0) {
			cheatProtection.values.blockFragmentsGenerations--;
			cheatProtection.values.blockedFragmentsLastRound = true;
		} else {
			cheatProtection.values.blockedFragmentsLastRound = false;
		}
		
		//deactivate cheat protection
		if(cheatProtection.values.blockFragmentsGenerations == 0){
			cheatProtection.values.blockFragments = false;
		} else {
			//reminder
			notice = engine.texts.displayText("gui","cheatprotection001");
			notice = notice.replace("###1###", cheatProtection.values.blockFragmentsGenerations);
			displayInfoMessage(notice);
		}
		
		//#################
		//ResetNGetTheFrags
		//#################
		
		var currentRule = engine.rulesOfEngagement;
		
		var ruleCondition = (statistics.game.currentGenerations < engine.gridRows);
		
		if(currentRule > 0){
			ruleCondition = false;
		}
		
		
		//resetting alot under 10 generations - no update performed
		if(progress.shopsystem.gui.displayFragments && statistics.game.currentGenerations > 0 && ruleCondition && !shopsystem.values.updateCall){
			cheatProtection.values.fastResets++;
			
			//check exploiting
			cheatProtection.checkExploits();
		}
		//decrease exploiting when more than gridsize
		if(statistics.game.currentGenerations > (engine.gridRows + engine.gridColumns)){
			cheatProtection.values.fastResets--;
			
			//min value is 0
			if(cheatProtection.values.fastResets < 0){
				cheatProtection.values.fastResets = 0;
			}
		}
		
		//console.log("fastResets: "+ cheatProtection.values.fastResets);
	}
}
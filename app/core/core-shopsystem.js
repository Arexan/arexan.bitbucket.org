var shopsystem = {
	//main values of the shopsystem
	values: {
		currentFragments: 0,
		fragementBonusEmptyGrid: 20,
		fragementsLastGeneration: 0,
		fragmentsPerGeneration: 1,
		shopButtonsChanged: true,
		updateCall: false
	},
	shops: new Array(),
	//buy the specified shop
	buy: function(shopName){
		//check if index is given for update
		if(shopName.indexOf("-") != -1){
			//split up values
			shopSplit = shopName.split("-");
			//get name and index
			shopName = shopSplit[0];
			shopIndex = shopSplit[1];
		
			shopsystem.shops[shopName].update(shopIndex);
		} else {
			shopsystem.shops[shopName].update();
		}
		
		shopsystem.values.updateCall = true;
	},
	//get the fragment count for the current generation or empty grid
	getFragmentsPerGeneration: function(emptyGrid, setStatistics){
		var tempFragments = 0;
	
		//cheatprotection
		if(cheatProtection.values.blockFragments){
			return 0;
		}
	
		//emptygrid bonus
		if(emptyGrid){
			tempFragments = (shopsystem.shops['grid'].values.index+1) *
					(shopsystem.values.fragementBonusEmptyGrid * 
						(shopsystem.values.fragmentsPerGeneration 
							+ shopsystem.shops['base'].values.fragmentBase
							+ shopsystem.shops['generationbonus'].getBonusAmount()
						)
					);
		
			//set statistics
			if(setStatistics){
				statistics.shopsystem.fragmentsOverall += tempFragments;
				statistics.shopsystem.fragmentsLastGame += tempFragments;
			}
		
			return tempFragments;
		} 
		//normal fragment
		else {	
			tempFragments = (shopsystem.values.fragmentsPerGeneration 
						+ shopsystem.shops['base'].values.fragmentBase
						+ shopsystem.shops['generationbonus'].getBonusAmount());
			
			//set statistics
			if(setStatistics){
				statistics.shopsystem.fragmentsOverall += tempFragments;
				statistics.shopsystem.fragmentsLastGame += tempFragments;
			}
			
			return tempFragments;
		}
	},
	//display all shop buttons
	displayShops: function(){
		var outputArray = new Array();
		var output = "";
		var buttonClass = "paybutton";
		
		//fragments available
		if(progress.shopsystem.gui.displayFragments){
		
			//turbo
			if(shopsystem.shops['turbo'].values.available){
				//display html
				output += shopsystem.shops['turbo'].displayActivation();
			}
		
			//get output from shops
			Object.keys(shopsystem.shops).forEach(
				function(key, index) {
					var element = shopsystem.shops[key];
					
					//shop is visible
					if(element.visible){
						outputArray[element.priority] = element.display();
					}
				}
			);
			
			//display in priority
			outputArray.forEach(
				function(key, index) {
					output += key;
				}
			);	
			
			//display shops only when something changed
			if(shopsystem.values.shopButtonsChanged){
				elements.setHTML(elementGenerator.getElementByAlias("shopsystem-buttons"), output);
			
				shopsystem.values.shopButtonsChanged = false;
			}
		}
	},
	//check progress of the shops
	checkProgress: function(){
		Object.keys(shopsystem.shops).forEach(
			function(key, index) {
				var element = shopsystem.shops[key];
				
				//progress reached
				if(!element.visible && shopsystem.values.currentFragments >= element.pricing[0]){
					element.visible = true;
				}
			}
		);
	}
}

//statistics
statistics.shopsystem = {
	fragmentsOverall: 0,
	fragmentsLastGame: 0,
	updatesBought: 0
}

//progress
progress.shopsystem = {
	gui: {
		displayUpdatesStatistic: false,
		displayFragments: false
	}
}
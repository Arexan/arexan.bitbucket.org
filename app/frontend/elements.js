var elements = {
	addEventListener: function(id, event, action, parameters){
		document.getElementById(id).addEventListener(event, function(event) {
			if(parameters == false || typeof(parameters) === "undefined") {
				//no parameters
				elements.callWindow(action, "");
			} else {
				elements.callWindow(action, parameters);
			}
		}, false);
	},
	addClass: function(id, value){
		document.getElementById(id).className += " "+value;
	},
	addTooltip: function(id, value){
		document.getElementById(id).title = value;
	},
	callWindow: function(action, parameters){
		var objectAction = action.split(".");
		//single parameter
		if(typeof(parameters) !== 'object'){
			switch((objectAction.length - 1)){
				case 0: window[objectAction](parameters); break;
				case 1: window[objectAction[0]][objectAction[1]](parameters); break;
				case 2: window[objectAction[0]][objectAction[1]][objectAction[2]](parameters); break;
				case 3: window[objectAction[0]][objectAction[1]][objectAction[2]][objectAction[3]](parameters); break;
				case 4: window[objectAction[0]][objectAction[1]][objectAction[2]][objectAction[3]][objectAction[4]](parameters); break;
				case 5: window[objectAction[0]][objectAction[1]][objectAction[2]][objectAction[3]][objectAction[4]][objectAction[5]](parameters); break;
				case 6: window[objectAction[0]][objectAction[1]][objectAction[2]][objectAction[3]][objectAction[4]][objectAction[5]][objectAction[6]](parameters); break;
				case 7: window[objectAction[0]][objectAction[1]][objectAction[2]][objectAction[3]][objectAction[4]][objectAction[5]][objectAction[6]][objectAction[7]](parameters); break;
				case 8: window[objectAction[0]][objectAction[1]][objectAction[2]][objectAction[3]][objectAction[4]][objectAction[5]][objectAction[6]][objectAction[7]][objectAction[8]](parameters); break;
				case 9: window[objectAction[0]][objectAction[1]][objectAction[2]][objectAction[3]][objectAction[4]][objectAction[5]][objectAction[6]][objectAction[7]][objectAction[8]][objectAction[9]](parameters); break;
			}
		} else {
			//multiple parameters
		}
	},
	deleteClass: function(id, classname){
		document.getElementById(id).className = document.getElementById(id).className.replace(classname, "");
	},
	setActive: function(id, classname){
		if(classname != false && classname != ""){
			document.getElementById(id).className = document.getElementById(id).className.replace(classname+"-off", classname);
		} else {
			document.getElementById(id).className = document.getElementById(id).className.replace("-off", "");
		}
	},
	setClasses: function(id, value){
		document.getElementById(id).className = value;
	},
	setFocus: function(id, classname){
		if(classname != false && classname != ""){
			document.getElementById(id).className = document.getElementById(id).className.replace(classname, classname+"-focus");
		} else {
			document.getElementById(id).className += "-focus";
		}
	},
	setHTML: function(id, value){
		//if the value changed
		if(document.getElementById(id).innerHTML !== value){
			document.getElementById(id).innerHTML = value;
		}
	},
	unsetActive: function(id, classname){
		if(classname != false && classname != ""){
			document.getElementById(id).className = document.getElementById(id).className.replace(classname, classname+"-off");
		} else {
			document.getElementById(id).className += "-off";
		}
	},
	unsetFocus: function(id, classname){
		if(classname != false && classname != ""){
			document.getElementById(id).className = document.getElementById(id).className.replace(classname+"-focus", classname);
		} else {
			document.getElementById(id).className = document.getElementById(id).className.replace("-focus", "");
		}
	}
}
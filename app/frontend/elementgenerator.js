var elementGenerator = {
	values: {
		newestPosition: 1
	},
	aliases: {
	
	},
	addElementBeforeOrAfter: function(id, before, alias){
		var siblingElement = document.getElementById(id);
		var newElementName = "element-"+ this.values.newestPosition;
		
		//new element => prevents  losing eventlisteners
		var newElement = document.createElement("DIV");
		newElement.id = newElementName;
		this.values.newestPosition++;
		
		//before or after
		if(before){
			siblingElement.parentNode.insertBefore(newElement, siblingElement.previousSibling);
		} else {
			siblingElement.parentNode.insertBefore(newElement, siblingElement.nextSibling);
		}
		
		//save alias for later use
		if(alias != false){
			this.aliases[alias] = newElementName;
		}
		
		//return the new element
		return newElementName;
	},
	addElement: function(position, prepend, alias){
		var editableElement = document.getElementById(position);
		var newElementName = "element-"+ this.values.newestPosition;
		
		//new element => prevents  losing eventlisteners
		var newElement = document.createElement("DIV");
		newElement.id = newElementName;
		this.values.newestPosition++;
		
		//prepend or append
		if(prepend){
			editableElement.insertBefore(newElement, editableElement.childNodes[0]);
		} else {
			//check if clearfix is given
			if(editableElement.lastChild == null || editableElement.lastChild.className == "clearfix"){
				editableElement.insertBefore(newElement, editableElement.lastChild);
			} else {
				editableElement.appendChild(newElement);
			}
		}
		
		//save alias for later use
		if(alias != false){
			this.aliases[alias] = newElementName;
		}
		
		//return the new element
		return newElementName;
	},
	addElementWithClassname: function(position, prepend, alias, classname){
		var elementName = elementGenerator.addElement(position, prepend, alias);
		elements.addClass(elementName, classname);
		
		return elementName;
	},
	getElementByAlias: function(alias){
		var returnElement = this.aliases[alias]
		
		if(returnElement != "undefined"){
			return returnElement;
		} else {
			return -1;
		}
	}
}
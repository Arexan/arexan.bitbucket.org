var typingmodule = {
	values: {
		charStreak: 0,
		wordStreak: 0,
		failed: false,
		currentWordBonus: 0,
		currentWordId: false,
		currentCharPosition: 0,
		maxWordBonus: 10,
		fragmentValue: 2,
		element: false,
		highestStreak: 0
	},
	setRandomWord: function(){
		var oldWordId = this.values.currentWordId;
		this.values.currentWordId = Math.floor(Math.random() * Object.keys(engine.texts.values.languages[engine.texts.values.language].typingmodule).length);
		
		//not the same word again
		if(oldWordId == this.values.currentWordId){
			this.setRandomWord();
		}
	},
	checkCurrentWord: function(keyvalue){
		//no word selected
		if(this.values.currentWordId === false){
			this.setRandomWord();
		}
		
		//get current word
		var word = engine.texts.displayText("typingmodule", this.values.currentWordId);
		
		//check keyvalue
		if(keyvalue == word.charAt(this.values.currentCharPosition)){
			//console.log(word +": "+ keyvalue +" == "+ word.charAt(this.values.currentCharPosition) +" -> correct");
			this.values.charStreak++;
			this.values.currentCharPosition++;
		} else {
			//console.log(word +": "+ keyvalue +" == "+ word.charAt(this.values.currentCharPosition) +" -> false");
			this.values.charStreak = 0;
			this.values.failed = true;
			this.values.currentCharPosition++;
		}
		
		//check if word is finished
		if(word.length <= this.values.currentCharPosition){
			//check if word was correct
			if(this.values.failed === false){
				this.values.wordStreak++;
				
				//check highscore
				if(this.values.highestStreak < this.values.wordStreak){
					this.values.highestStreak = this.values.wordStreak;
				}
			} else {
				this.values.wordStreak = 0;
			}
			
			//bonus value
			var fragmentBonusValue = this.getBonusAmount(false);
			
			//reset value
			this.values.charStreak = 0;
			this.values.currentCharPosition = 0;
			this.values.failed = false;
			this.setRandomWord();
			
			//get fragments for correct word
			console.log(fragmentBonusValue);
			shopsystem.values.currentFragments += fragmentBonusValue;
			displayScoreboardGUI();
		}
		
		this.visualize(word);
	},
	getBonusAmount: function(preview){
		var word = engine.texts.displayText("typingmodule", this.values.currentWordId);
		var streakBonus = this.values.wordStreak;
		
		//check cap
		if(streakBonus > this.values.maxWordBonus){
			streakBonus = this.values.maxWordBonus;
		}
		
		//preview for next bonus
		if(preview){
			return (this.values.fragmentValue * word.length) * (streakBonus + 1)
		}
		
		return (this.values.fragmentValue * word.length) * streakBonus;
	},
	visualize: function(){
		//check if the element is already given
		if(this.values.element === false){
			//add main element
			this.values.element = elementGenerator.addElementWithClassname("page-bottom", false, "typingmodule", "typing-"+ gui.values.currentLayout);
			//add tooltip
			elements.addTooltip(this.values.element, engine.texts.displayText("gui", "tooltip001"));
			
			//add bonus element
			elementGenerator.addElementWithClassname(this.values.element, false, "typingmodule-bonus", "bonus-"+ gui.values.currentLayout);
			
			//add target element
			elementGenerator.addElementWithClassname(this.values.element, false, "typingmodule-target", "target-"+ gui.values.currentLayout);
			
			//add streak element
			elementGenerator.addElementWithClassname(this.values.element, false, "typingmodule-streak", "streak-"+ gui.values.currentLayout);
		}
		
		//get current word
		var word = engine.texts.displayText("typingmodule", this.values.currentWordId);
		
		//cut word in characters
		var target = "";
		for (var i = 0; i < word.length; i++) {
			//check if char is the next to be typed
			if(i == typingmodule.values.currentCharPosition){
				target += "<span class=\"highlighted-"+ gui.values.currentLayout +"\">"+ word.charAt(i) +"</span>";
			} else {
				target += word.charAt(i)
			}
		}
		
		//add failure color
		if(this.values.failed){
			target = "<span class=\"failure-"+ gui.values.currentLayout +"\">"+ target +"</span>";
		}
		
		//show bonus
		var bonus = engine.texts.displayText("gui", "ui010").replace("###1###", this.getBonusAmount(true));
		
		//add streak
		var streak = "";
		if(this.values.wordStreak > 1 && this.values.failed === false){
			streak = engine.texts.displayText("gui", "ui011") + this.values.wordStreak;
		}
		
		//set html
		elements.setHTML(elementGenerator.getElementByAlias("typingmodule-target"), target);
		elements.setHTML(elementGenerator.getElementByAlias("typingmodule-bonus"), bonus);
		elements.setHTML(elementGenerator.getElementByAlias("typingmodule-streak"), streak);
	}
}
# CONWAY'S GAME OF LIFE

Different approach to Conway's Game Of Life, implemented in HTML and JavaScript.  
Supports:

* Achievements
* Statistics and Highscores
* Improvable bonuses
* Keyboard / Mouse
* Multilanguage

## BUGs


## TODO's

* Coin bag?
* New Games (Snake, Pong)
* Toolbar / Shortinfo
* Save progress in html5 local cache
* Design Shop
* Click Counter => Fragments every X clicks


## IMPLEMENTED

14.03.2016

* Typing Bonus added - Arexan

11.03.2016

* Turbo Shop added - Arexan
* Rules Shop added - Arexan

10.03.2016

* Tooltips for each element (Shop Buttons) - Arexan

09.03.2016

* Block F5 or Backspace - Arexan

26.02.2016

* Added Multilanguage support - Arexan

25.02.2016

* Added the prestige shop (for players with too much fragments) - Arexan

24.02.2016:

* Exploit prevention (resetting alot) IMPLEMENTED - Arexan
* First row not working properly sometimes? FIXED - Baschan
* Shop Buttons are only updated on changes now - Arexan
* Buyable Fragment counter statistic (Displayed in History for each game) - Arexan

19.02.2016:  

* Generationbonus Rework (Scaling with updates from low to higher values) - Arexan
* Add Keyboard recognition (space + num) - Arexan
